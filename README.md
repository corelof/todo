# TODO #

##### TODO is utility for recursive checking files for forgotten "todo" comments.

---

### Installing ###

- Download
    1. [Download binary (linux only)](https://bitbucket.org/corelof/todo/downloads/todo)
    2. Make sure that binary file is inside of ```$PATH```
- Build from sources for other platforms
    1. [Insall golang](https://golang.org/dl/)
    2. Clone repo into ```$GOPATH/src```
    3. Run ```go install``` inside of ```$GOPATH/src/CLONED_REPO```
    4. Add ```$GOPATH/bin``` to ```$PATH``` or copy ```$GOPATH/todo``` into the folder contained in the path

---

### Usage ###

- Simply type ```todo``` in terminal inside of needed directory
- You will see list of lines containing "todo" keyword in files inside of this directory, it`s subdirectories, etc. 

---

### Team ###


 @corelof
